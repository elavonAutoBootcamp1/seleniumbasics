package com.elavon.tutorial;

import static org.junit.Assert.assertEquals;

import org.junit.Ignore;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class WebdriverSample {

    public WebDriver openApplication(String browser, String url) {

        WebDriver driver = null;

        if ("Chrome".equals(browser)) {
            System.setProperty("webdriver.chrome.driver",
                "I:\\git\\seleniumbasics\\testMavenJunit\\drivers\\chromedriver.exe");
            driver = new ChromeDriver();
        }
        else {
            System.setProperty("webdriver.gecko.driver",
                "I:\\git\\seleniumbasics\\testMavenJunit\\drivers\\geckodriver_32.exe");
            driver = new FirefoxDriver();
        }

        driver.get(url);

        if ("Chrome".equals(browser)) {
            driver.manage().window().maximize();
        }

        return driver;
    }

    @Ignore
    @Test
    public void ts01() {

        String browser = "Chrome";
        String url = "http://www.wikipedia.com";
        String expectedTitle = "Wikipediaasdsadsa";

        WebDriver driver = openApplication(browser, url);
        String actualTitle = driver.getTitle();
        System.out.println(actualTitle);
        assertEquals(expectedTitle, actualTitle);
        driver.close();
    }

    @Ignore
    @Test
    public void ts02() {

        String browser = "Chrome";
        String url = "http://www.wikipedia.com";
        String expectedTitle = "Wikipedia";

        WebDriver driver = openApplication(browser, url);

        String tagName = driver.findElement(By.id("searchInput")).getTagName();
        driver.findElement(By.id("searchInput")).sendKeys("Elavon");
        driver.findElement(By.cssSelector(".pure-button")).click();

        System.out.println(tagName);

    }

    @Test
    public void ts03() {

        String browser = "Chrome";
        String url = "http://demoqa.com/registration/";

        WebDriver driver = openApplication(browser, url);

        // driver.findElement(By.id("name_3_firstname")).sendKeys("Test First name");
        // driver.findElement(By.name("last_name")).sendKeys("test last name");
        // driver.findElement(By.cssSelector("li.fields:nth-child(2) > div:nth-child(1) > div:nth-child(2) >
        // input:nth-child(6)")).click();

        WebElement firstNameTextbox = driver.findElement(By.id("name_3_firstname"));
        WebDriverWait myWaitVar = new WebDriverWait(driver, 10);

        myWaitVar.until(ExpectedConditions.visibilityOf(firstNameTextbox));

        String firstName = "test first name";
        typeInto(firstNameTextbox, firstName);
    }

    public void typeInto(WebElement element, String value) {

        element.clear();
        element.sendKeys(value);
    }

    @Ignore
    @Test
    public void ts04() {

        String browser = "Chrome";
        String url = "https://www.wikipedia.org/";

        WebDriver driver = openApplication(browser, url);

        // List<WebElement> languages = driver.findElements(By.className("link-box"));
        //
        // for (WebElement webElement : languages) {
        // System.out.println(webElement.getText());
        // }

        System.out.println(driver.findElement(By.partialLinkText("5 443 000+")).getText());

        driver.findElement(By.linkText("Privacy Policy")).click();

    }

}
