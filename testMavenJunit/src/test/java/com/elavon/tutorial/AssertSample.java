/**
 * 
 */
package com.elavon.tutorial;

import static org.junit.Assert.*;

import org.junit.Ignore;
import org.junit.Test;

/**
 * @author Bren Buenaventura
 * @since Jul 21, 2017
 */
public class AssertSample {

    // test data
    String str1 = new String("abc");

    String str2 = new String("abc");

    String str3 = null;

    String str4 = "abc";

    String str5 = "abc";

    int val1 = 5;

    int val2 = 6;

    String[] expectedArray = { "ones", "two", "three" };

    String[] resultArray = { "one", "two", "three" };
    
    String expected = "cat";
    String actual = "dog";
    
    @Ignore
    @Test
    public void assertEqualsSample() {

    	
        // Check that two objects are equal
    	assertEquals("cat", actual);
        assertEquals(str1, str2);
    }

    @Ignore
    @Test
    public void assertTrueSample() {
        // Check that a condition is true
        assertTrue(val1 < val2);
        assertTrue(expected.equals(actual));

    }

    @Ignore
    @Test
    public void assertFalseSample() {

        // Check that a condition is false
        assertFalse(val1 > val2);
        assertFalse(expected.equals(actual));
    }

    @Ignore
    @Test
    public void assertNotNullSample() {

        // Check that an object isn't null
        assertNotNull(str1);
        assertNotNull(str3);
    }

    @Ignore
    @Test
    public void assertNullSample() {

        // Check that an object is null
        assertNull(actual);
    }

    @Ignore
    @Test
    public void assertSameSample() {

    	String a = "";
    	String b = null;
    	
        // Check if two object references point to the same object
        assertSame(str4, str5);
        assertSame(a, b);
    }

    @Ignore
    @Test
    public void assertNotSameSample() {

    	String a = "";
    	String b = null;
        // Check if two object references not point to the same object
        assertNotSame(str1, str3);
        assertNotSame(a, b);
    }

//    @Ignore
    @Test
    public void assertArrayEqualsSample() {

        // Check whether two arrays are equal to each other.
        assertArrayEquals(expectedArray, resultArray);
    }
}
