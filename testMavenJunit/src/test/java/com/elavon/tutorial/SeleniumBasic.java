/**
 * 
 */
package com.elavon.tutorial;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.Ignore;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;

/**
 * @author Bren Buenaventura
 * @since Jul 25, 2017
 */
public class SeleniumBasic {

    public WebDriver openWebsite(String url) {

        System.setProperty("webdriver.gecko.driver", "I:\\git\\seleniumbasics\\testMavenJunit"
        		+ "\\drivers\\geckodriver_32.exe");
//         System.setProperty("webdriver.chrome.driver", "I:\\git\\seleniumbasics\\testMavenJunit"
//         		+ "\\drivers\\chromedriver.exe");

        WebDriver driver = new FirefoxDriver();
//        WebDriver driver = new ChromeDriver();

        driver.get(url);
        driver.manage().window().maximize();

        return driver;
    }

//    @Ignore
    @Test
    public void testScript01() {

        String url = "http://www.google.com";
        String expectedTitle = "Google";

        WebDriver driver = openWebsite(url);
        String actualTitle = driver.getTitle();
        assertEquals(expectedTitle, actualTitle);
        driver.close();
    }

    @Ignore
    @Test
    public void testScript2() {

        String url = "http://www.google.com";
        WebDriver driver = openWebsite(url);
        String tagName = driver.findElement(By.name("q")).getTagName();
        System.out.println(tagName);
        driver.close();
        System.exit(0);
    }

    @Test
    public void testScript3() {

        String url = "https://www.wikipedia.org/";

        WebDriver driver = openWebsite(url);

        List<WebElement> languages = driver.findElements(By.className("link-box"));
        for (WebElement webElement : languages) {
            System.out.println(webElement.getText());
        }

//        String searchFor = "Elavon";
//        WebElement searchTextbox = driver.findElement(By.id("searchInput"));
//        searchTextbox.sendKeys(searchFor);
//
//        Select languageDropdown = new Select(driver.findElement(By.id("searchLanguage")));
//        languageDropdown.selectByVisibleText("Espa�ol");
//        languageDropdown.selectByIndex(0);
//        languageDropdown.selectByValue("en");
//
//        WebElement languageLabel = driver.findElement(By.id("jsLangLabel"));
//        String selectedLanguage = languageLabel.getText();
//        System.out.println("Selected Language: " + selectedLanguage);
//
//        WebElement searchButton = driver.findElement(By.cssSelector(".pure-button"));
//        searchButton.click();
//
//        System.out.println(driver.getTitle());
//
//        driver.close();
//        System.exit(0);
    }

}
