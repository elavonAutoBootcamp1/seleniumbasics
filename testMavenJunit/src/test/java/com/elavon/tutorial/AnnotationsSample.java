package com.elavon.tutorial;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

/**
 * @author Bren Buenaventura
 * @since Jul 19, 2017
 * @version 1.0
 */
public class AnnotationsSample {

    @BeforeClass
    public static void beforeClassSample() {

        System.out.println("This is the before class annotation");
    }

    @Before
    public void beforeSample() {

        System.out.println("This is the before annotation");
    }

    @Test
    public void testScript1() {

        System.out.println("Test Script 1 executed");
    }

    @Test
    public void testScript2() {

        System.out.println("Test Script 2 executed");
    }

    @Ignore
    @Test
    public void testScript3() {

        System.out.println("Test Script 3 executed");
    }

    @After
    public void afterSample() {

        System.out.println("This is the after annotation");
    }

    @AfterClass
    public static void afterClassSample() {

        System.out.println("This is the after class annotation");
    }
}
