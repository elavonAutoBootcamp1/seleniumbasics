package com.elavon.tutorial;

import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class WebDriverSample2 {

    public WebDriver openFirefox(String url) {

        System.setProperty("webdriver.gecko.driver", "drivers/geckodriver_32.exe");
        // System.setProperty("webdriver.chrome.driver",
        // "C:\\Users\\bnbuenaventura\\Documents\\gitworkspace\\seleniumbasics\\testMavenJunit\\drivers\\chromedriver.exe");

        WebDriver driver = new FirefoxDriver();
        // WebDriver driver = new ChromeDriver();

        driver.get(url);
        // driver.manage().window().maximize();

        String actualTitle = driver.getTitle();

        System.out.println(actualTitle);

        return driver;
    }

    @Test
    public void testScript01() {

        String url = "http://www.demoqa.com/registration";

        WebDriver driver = openFirefox(url);
        driver.close();
    }

}
