/**
 * 
 */
package com.elavon.tutorial.utils;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * @author Bren Buenaventura
 * @since Jul 27, 2017
 */
public class PageUtils {

    public WebDriver openApplication(String browser, String url) {

        WebDriver driver = null;

        if ("Chrome".equals(browser)) {
            System.setProperty("webdriver.chrome.driver",
                "I:\\git\\seleniumbasics\\testMavenJunit\\drivers\\chromedriver.exe");
            driver = new ChromeDriver();
        }
        else {
            System.setProperty("webdriver.gecko.driver",
                "I:\\git\\seleniumbasics\\testMavenJunit\\drivers\\geckodriver_32.exe");
            driver = new FirefoxDriver();
        }

        driver.get(url);

        if ("Chrome".equals(browser)) {
            driver.manage().window().maximize();
        }

        return driver;
    }

    public void waitForWebElemet(WebDriver driver, WebElement element) {

        WebDriverWait myWaitVar = new WebDriverWait(driver, 10);
        myWaitVar.until(ExpectedConditions.visibilityOf(element));
    }
}
